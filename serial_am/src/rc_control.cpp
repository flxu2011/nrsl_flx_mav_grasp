#include "ros/ros.h"
#include "ros/time.h"
#include <string>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <unistd.h>
#include <vector>
#include <mavros_msgs/RCIn.h>
#include <serial_am/control_state.h>

enum chan{c1,c2,c3,c4,c5,c6,c7,c8};
uint16_t channel[8]={0};

enum offboard_state{Normal=1,offboard_turn_on=2,Landing=4,AM_back=8,offboard_turn_off=16};



void RC_read_Callback(mavros_msgs::RCIn RCmsg)
{

	for (int i = 0; i < RCmsg.channels.size(); ++i)
		channel[i]=RCmsg.channels[i];
}



int main(int argc, char* argv[])
{
	ros::init(argc, argv, "rc_control_node");
  	ros::NodeHandle n;
  	ros::Subscriber sub_pose_bang1 = n.subscribe("/mavros/rc/in", 1, RC_read_Callback);
  	ros::Publisher control_state_puber = n.advertise<serial_am::control_state>("/flx_control_state",10);
  	

  	serial_am::control_state control_msg;
  	control_msg.state=Normal;
  	uint8_t last_state=Normal;
  	uint16_t last_c7=1000;
  	ros::Rate loop_rate(20);
  	while (ros::ok())
	{
		if(channel[c1])
		{
			//printf("%d %d %d %d %d %d %d %d\n",channel[c1],channel[c2],channel[c3],channel[c4],channel[c5],channel[c6],channel[c7],channel[c8]);
			if (channel[c7]>1600)control_msg.state=offboard_turn_on;

			if (channel[c8]>1600&&channel[c7]>1600)control_msg.state=Landing;//land
			if (channel[c7]<1600&&last_c7>1600)control_msg.state=offboard_turn_off;//am allways back
			
			if (channel[c6]>1600)control_msg.state|=AM_back;//am always back


			if (control_msg.state!=last_state)control_msg.amlock=1;
			else control_msg.amlock=0;

			last_state=control_msg.state;
			last_c7=channel[c7];

			control_state_puber.publish(control_msg);

		}
		ros::spinOnce();
		loop_rate.sleep();
	}

 }