#include "ros/ros.h"
#include "ros/time.h"
#include <string>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <unistd.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>
#include <vector>

using namespace std;


double bang1_x,bang1_y,bang1_z,bang1_t;
double bang2_x,bang2_y,bang2_z,bang2_t;
double mav_x,mav_y,mav_z,mav_t;

struct posxy
{
	double x;
	double y;
};

  vector<posxy> solution_1;
  vector<posxy> solution_2;


void func_pose_bang1(const geometry_msgs::PoseStampedConstPtr &msg)
{
	// double roll, pitch, yaw;
	// tf::Quaternion q;
	// tf::quaternionMsgToTF(msg->pose.orientation, q);
 	// tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
  	bang1_x=msg->pose.position.x;
  	bang1_y=msg->pose.position.y;
  	bang1_z=msg->pose.position.z;
  	bang1_t=msg->header.stamp.toSec();

}
void func_pose_bang2(const geometry_msgs::PoseStampedConstPtr &msg)
{
  	bang2_x=msg->pose.position.x;
  	bang2_y=msg->pose.position.y;
  	bang2_z=msg->pose.position.z;
  	bang2_t=msg->header.stamp.toSec();
}


void compute_pre_position(double d,double &x_solution_1,double &y_solution_1,double &x_solution_2,double &y_solution_2)
{
	double x1,y1,x2,y2;
	x1=bang1_x;y1=bang1_y;
	x2=bang2_x;y2=bang2_y;
	double s1,sqrt_s1;
	s1=1.0/(x1*x1 - 2.0*x1*x2 + x2*x2 + y1*y1 - 2.0*y1*y2 + y2*y2);
	sqrt_s1=sqrt(s1);
	x_solution_1=-(2.0*y1*(y1/2.0 + y2/2.0 + d*x1*sqrt_s1 - d*x2*sqrt_s1) - 2.0*y2*(y1/2.0 + y2/2.0 + d*x1*sqrt_s1 - d*x2*sqrt_s1) - x1*x1 + x2*x2 - y1*y1 + y2*y2)/(2.0*x1 - 2.0*x2);
	x_solution_2=-(2.0*y1*(y1/2.0 + y2/2.0 - d*x1*sqrt_s1 + d*x2*sqrt_s1) - 2.0*y2*(y1/2.0 + y2/2.0 - d*x1*sqrt_s1 + d*x2*sqrt_s1) - x1*x1 + x2*x2 - y1*y1 + y2*y2)/(2.0*x1 - 2.0*x2);
	y_solution_1=y1/2.0 + y2/2.0 + d*x1*sqrt_s1 - d*x2*sqrt_s1;
	y_solution_2=y1/2.0 + y2/2.0 - d*x1*sqrt_s1 + d*x2*sqrt_s1;


}

void Local_pose_CallBack(const geometry_msgs::PoseStampedConstPtr &msg)
{
	mav_x=msg->pose.position.x;
  	mav_y=msg->pose.position.y;
  	mav_z=msg->pose.position.z;
  	mav_t=msg->header.stamp.toSec();

}


double distance_bt_point2D(double x1,double y1,double x2,double y2)
{
	return (y1-y2)*(y1-y2)+(x1-x2)*(x1-x2);
}

int main(int argc, char* argv[])
{
	ros::init(argc, argv, "flx_path_planner");
  	ros::NodeHandle n;


  	ros::Subscriber sub_pose_bang1 = n.subscribe("/vrpn_client_node/bang1/pose", 1, func_pose_bang1);
  	ros::Subscriber sub_pose_bang2 = n.subscribe("/vrpn_client_node/bang2/pose", 1, func_pose_bang2);
  	ros::Subscriber local_pose_suber=n.subscribe("/mavros/vision_pose/pose",1,Local_pose_CallBack);///mavros/vision_pose/pose
  	ros::Publisher Path_puber = n.advertise<nav_msgs::Path>("/flx_path_plan",1);
  	ros::Publisher item_puber = n.advertise< geometry_msgs::Point>("/bang_center",1);

  	bool is_track_valid=false;
  	bool is_track_valid_last=false;
  	double current_t;
	posxy solution_1_date,solution_2_date;
	vector<posxy> current_solution;
  	ros::Rate loop_rate(100);
  	while (ros::ok())
	{
		current_t=ros::Time::now().toSec();
		if ((current_t-bang1_t)<0.2&&(current_t-bang2_t)<0.2&&(current_t-mav_t)<0.2)is_track_valid=true;
		else is_track_valid=false;


		if (is_track_valid)
		{
			if (is_track_valid_last==false)ROS_INFO("Valid track. Doing path plan...");
			geometry_msgs::Point item_center;
			item_center.x=bang1_x/2.0+bang2_x/2.0;
			item_center.y=bang1_y/2.0+bang2_y/2.0;
			item_center.z=bang1_z/2.0+bang2_z/2.0;
			item_puber.publish(item_center);

			solution_1.clear();
			solution_2.clear();
			for (double d = 2.0; d > 0.2; d-=0.1)
			{
				compute_pre_position(d,solution_1_date.x,solution_1_date.y,solution_2_date.x,solution_2_date.y);
				solution_1.push_back(solution_1_date);
				solution_2.push_back(solution_2_date);
				//printf("s1(%f,%f) s2(%f,%f)\n",solution_1_date.x,solution_1_date.y,solution_2_date.x,solution_2_date.y);
			}
			current_solution=(  distance_bt_point2D(mav_x,mav_y,solution_1_date.x,solution_1_date.y)
							   <distance_bt_point2D(mav_x,mav_y,solution_2_date.x,solution_2_date.y)
							  )?solution_1:solution_2;
			double fai,theta,psai,qw,qx,qy,qz;
			fai=0.0;theta=0.0;
			psai=atan2(bang1_y/2.0+bang2_y/2.0-solution_1_date.y,bang1_x/2.0+bang2_x/2.0-solution_1_date.x);
			qw=cos(fai/2)*cos(theta/2)*cos(psai/2)+sin(fai/2)*sin(theta/2)*sin(psai/2);
			qx=sin(fai/2)*cos(theta/2)*cos(psai/2)-cos(fai/2)*sin(theta/2)*sin(psai/2);
			qy=cos(fai/2)*sin(theta/2)*cos(psai/2)+sin(fai/2)*cos(theta/2)*sin(psai/2);
			qz=cos(fai/2)*cos(theta/2)*sin(psai/2)-sin(fai/2)*sin(theta/2)*cos(psai/2);

			//printf("---\nx:%f\ny:%f\nz:%f\nw:%f\n",qx,qy,qz,qw);

			nav_msgs::Path m_path;
			m_path.header.stamp = ros::Time::now();
			m_path.header.frame_id = "world";
			for (int i = 0; i < current_solution.size(); ++i)
			{
				geometry_msgs::PoseStamped pose;
				pose.header.stamp = ros::Time::now();
		        pose.header.frame_id = "fcu";
		        pose.pose.orientation.x = qx;
		        pose.pose.orientation.y = qy;
		        pose.pose.orientation.z = qz;
		        pose.pose.orientation.w = qw;
		        pose.pose.position.x = current_solution[i].x;
		        pose.pose.position.y = current_solution[i].y;
		        pose.pose.position.z = (bang1_z+bang2_z)/2.0+0.4;
				m_path.poses.push_back(pose);
			}
			Path_puber.publish(m_path);
		}
		else if(is_track_valid_last)
		{
			ROS_ERROR("Invalid track: Tracking lost");
		}

		is_track_valid_last=is_track_valid;
		ros::spinOnce();
		loop_rate.sleep();
	}

	return 0;

}