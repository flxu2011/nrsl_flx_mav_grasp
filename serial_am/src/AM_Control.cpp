#include "Kin.h"
#include "ros/ros.h"
#include <stdio.h>
#include "serial.h"
#include "serial_am/ikSrv.h"
#include "serial_am/graper.h"

// Create serial port
serial serial;

using namespace std;

bool do_init_pos=false;
bool do_back_pos=false;
bool do_auto_grasp=false;
int auto_grasp_time=0;


void Servo_cmd(int no,int pos,int time_value)
{
	if (pos>2500)pos=2500;if (pos<500)pos=500;
	char cmd[100];
	sprintf(cmd,"#%dP%dT%d\r\n",no,pos,time_value);
	serial.Write(cmd,strlen(cmd));
	//printf("%d %d\n",no,pos);
}

void Joint_cmd(double ang1,double ang2,double ang3,double ang4,int time_value)
{
	ang1=ang1*180.0/pi;
	ang2=ang2*180.0/pi;
	ang3=ang3*180.0/pi;
	ang4=ang4*180.0/pi;

	int pwm1,pwm2,pwm3,pwm4;
	pwm1=(-11.444444444f)*ang1+540.0;
	Servo_cmd(1,pwm1,time_value);
	Servo_cmd(1,pwm1,time_value);
	pwm2=11.644444444f*ang2+450.0;//370
	Servo_cmd(2,pwm2,time_value);
	Servo_cmd(2,pwm2,time_value);
	pwm3=(-25/3)*(ang3+1.40*ang2)+2700;//2750
	Servo_cmd(3,pwm3,time_value);
	Servo_cmd(3,pwm3,time_value);
	pwm4=14.3222*ang4+1211.0;
	Servo_cmd(4,pwm4,time_value);
	Servo_cmd(4,pwm4,time_value);


}

void Graper_On(void)
{
	char cmd[100];
	sprintf(cmd,"H");
	serial.Write(cmd,strlen(cmd));
}

void Graper_Off(void)
{
	char cmd[100];
	sprintf(cmd,"J");
	serial.Write(cmd,strlen(cmd));
}

void Init_Pos(void)
{
	// Joint_cmd(  -pi/2,
	// 			pi/2.0,
	// 			pi/2.0,
	// 			pi/2.0,
	// 			1000);

	// Joint_cmd(-pi/3.0,pi/2.0,pi/2.0,pi/2.0,1000);
	

	Joint_cmd(-0.750657,2.95,-0.447010,1.570796,500);
}

void Back_Pos(void)
{
	// Joint_cmd(-pi/3.0,pi-0.2,-pi/3.0,0,500);
	// ros::Duration(0.6).sleep(); 

	// Joint_cmd(-pi/6,pi-0.2,-pi/3,0,500);
	// ros::Duration(0.6).sleep(); 
	Joint_cmd(-2.397917,2.007949,1.960764,1.570796,2000);

}



int Valid_Space(FLX_kinematics &Kin)
{

	for (int i = 0; i < 2; ++i)
	{
		if (
				(Kin.theta[i].theta1<5.0*pi/180.0)&&(Kin.theta[i].theta1>-150.0*pi/180.0)&&
				(Kin.theta[i].theta2>0)&&(Kin.theta[i].theta2<pi)//&&
				//(Kin->theta[i].theta3)&&(Kin->theta[i].theta3)
			)return i+1;
	}
	return 0;
	
}

bool ik_CallBack(serial_am::ikSrv::Request &msg,serial_am::ikSrv::Response &res)//注意：service阻塞
{
	FLX_kinematics Kin;
	double theta_e,theta4,px,py;
	theta_e=msg.theta_e;
	theta4=msg.theta4;
	px=msg.px;
	py=msg.py;

	if (py<-200)
	{
		do_init_pos=true;
		res.is_done=3;
		return true;
	}
	else if (py<-100)
	{
		do_back_pos=true;
		res.is_done=4;
		return true;
	}


	Kin.Inverse_Kinematics(	theta_e,theta4,px,py);
	if (Kin.theta.size())
	{
		int valid_n=Valid_Space(Kin);
		if (valid_n>0)
		{
			Kin.Show_Inverse_Result_Theta(Kin.theta[valid_n-1]);
			Joint_cmd(Kin.theta[valid_n-1].theta1,Kin.theta[valid_n-1].theta2,Kin.theta[valid_n-1].theta3,Kin.theta[valid_n-1].theta4,500);
		}
		else printf("Null solution.\n");
		res.is_done=valid_n;
	}
	else 
	{
		res.is_done=0;
		printf("Null solution.\n");
	}
	
	// for (int i=0;i<4;i++)
	// {
	// 	theta_e=0;
	// 	theta4=pi/2;
	// 	px=0.20;
	// 	py=-0.15;
	// 	printf("--------------------");
	// 	Kin.Inverse_Kinematics(	theta_e,theta4,px,py);
	// 	if (Kin.theta.size())
	// 	{
	// 		Kin.Show_Inverse_Result_Theta(Kin.theta[1]);
	// 		Joint_cmd(Kin.theta[1].theta1,Kin.theta[1].theta2,Kin.theta[1].theta3,Kin.theta[1].theta4,500);
	// 		Graper_Off();
	// 		ros::Duration(1.5).sleep(); 
	// 	}
	// 	else printf("Null solution.\n");
	// 	ros::Duration(0.8).sleep(); 
	// 	px=0.35;
	// 	printf("--------------------");
	// 	Kin.Inverse_Kinematics(	theta_e,theta4,px,py);
	// 	if (Kin.theta.size())
	// 	{
	// 		Kin.Show_Inverse_Result_Theta(Kin.theta[1]);
	// 		Joint_cmd(Kin.theta[1].theta1,Kin.theta[1].theta2,Kin.theta[1].theta3,Kin.theta[1].theta4,500);
	// 		Graper_On();
	// 		ros::Duration(1.5).sleep(); 
	// 	}
	// 	else printf("Null solution.\n");
	// 	ros::Duration(0.8).sleep(); 
	// }

	return true;
}

bool graper_CallBack(serial_am::graper::Request &msg,serial_am::graper::Response &res)
{
	if (msg.state==1)Graper_On();
	if (msg.state==0)Graper_Off();

	if (msg.state>1)
	{
		do_auto_grasp=true;
		auto_grasp_time=msg.state;
	}

	res.is_done=1;
	return true;
}




int main(int argc, char** argv)
{

	ros::init(argc,argv,"AM_servo_control");
	ros::NodeHandle n;
	std::string com_cs;
	n.param<std::string>("COM_dev",com_cs,"/dev/ttyUSB_CP210X");
	serial.Open((char*)com_cs.c_str(), 9600, 8, NO, 1);
	Init_Pos();
	ros::Duration(2.0).sleep();

	//ros::Subscriber Link_pose_suber=n.subscribe("/serial_am/Link_pose",1,ik_CallBack);
	ros::ServiceServer Link_pose_service = n.advertiseService("/serial_am/Link_pose", ik_CallBack);
	ros::ServiceServer Graper_service = n.advertiseService("/serial_am/graper", graper_CallBack);
	ros::Rate loop_rate(1000);
	while(ros::ok())
	{
		if (do_init_pos)
		{
			do_init_pos=false;
			Init_Pos();
		}
		if (do_back_pos)
		{
			do_back_pos=false;
			Back_Pos();
		}
		if (do_auto_grasp)
		{
			auto_grasp_time--;
			if (auto_grasp_time==0)
			{
				Graper_On();
				do_auto_grasp=false;
			}
		}


		ros::spinOnce();
		loop_rate.sleep();
	}

	serial.Close();



	
		
	
	
	return 0;
}
