#include "serial_am/ikSrv.h"
#include "serial_am/graper.h"
#include "ros/ros.h"

#define pi 3.141592653589793



int main(int argc,char** argv)
{
	ros::init(argc,argv,"control_puber");

// printf(   "You   have   inputed   total   %d   argments\n"   ,   argc   );  
 //   for( int  i=0   ;   i<argc   ;   i++)  
 //   {  
 //   printf(   "arg%d   :   %s\n"   ,   i   ,   argv[i]   );  
 //   }  

	ros::NodeHandle n;
	//ros::Publisher ik_puber=n.advertise<serial_am::ikMsg>("/serial_am/Link_pose",1);
	ros::ServiceClient ik_puber = n.serviceClient<serial_am::ikSrv>("/serial_am/Link_pose");
	ros::ServiceClient graper_puber = n.serviceClient<serial_am::graper>("/serial_am/graper");

	serial_am::ikSrv msg;
	serial_am::graper graper_action;
	msg.request.theta_e=0;
	msg.request.theta4=pi/2;
	msg.request.px=0.22;
	msg.request.py=-0.12;
	

	ik_puber.call(msg);
	if (msg.response.is_done)printf("Link done! Use %dnd solution.\n",msg.response.is_done);
	else printf("Null solution!\n");

	graper_action.request.state=0;//0  off 
	graper_puber.call(graper_action);
	if (graper_action.response.is_done)printf("graper off!\n");

	// ros::Duration(3.0).sleep();
	// graper_action.request.state=1;//1  on
	// graper_puber.call(graper_action);
	// if (graper_action.response.is_done)printf("graper on!\n");

	// bool t1=true;
	// ros::Rate loop_rate(1000);
	// while(ros::ok())
	// {
	// 	if (t1==true)
	// 	{
	// 		t1=false;
	// 		ik_puber.call(msg);
	// 		if (msg.response.is_done)
	// 		{
	// 			printf("done!\n");
	// 		}
	// 	}
		

	// 	ros::spinOnce();
	// 	loop_rate.sleep();
	// }




	return 0;
}