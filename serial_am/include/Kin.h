#include <cmath>
#include <vector>
#include <iostream>
#include <stdlib.h>

using namespace std;

#define  pi 3.14159265358f
#define  Per_PWM_Ang 11.444444444f 	//(1570-540)/90.0;


class FLX_kinematics
{	
	
	public:
		struct Theta{double theta1,theta2,theta3,theta4;};
		double r11,r12,r13;
		double r21,r22,r23;
		double r31,r32,r33;
		double t1,t2,t3;
		double nx,ox,ax,px;
		double ny,oy,ay,py;
		double nz,oz,az,pz;
		vector<Theta> theta;

		FLX_kinematics();
		~FLX_kinematics();
		void Forward_Kinematics(double angle1,double angle2,double angle3,double angle4);
		void Show_Forward_Result_RT(void);
		void Inverse_Kinematics(double nx0,double ox0,double ax0,double px0,
								double ny0,double oy0,double ay0,double py0,
								double nz0,double oz0,double az0,double pz0);
		void Inverse_Kinematics(double theta_e,double theta4,double px,double py);
		void Show_Inverse_Result_Theta(Theta theta_result);
		double Form_postive_negative_180(double ang);
		void Form_result(Theta &theta_result);

};