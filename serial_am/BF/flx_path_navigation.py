#!/usr/bin/env  python
# coding=utf-8


import rospy
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Float64
import copy
import math


class Flx_Path_wapper:
    def __init__(self):
        self.current_pose = PoseStamped()
        self.current_path = Path()
        self.last_path= Path()
        self.current_destination_pose =PoseStamped()
        self.__path_pointer=0
        self.frame_id = "fcu"
        self.__is_path_update =0
        self.start_locker=0
        self.path_in_use = Path()
        self.distance_err=0
    def reload_pose(self,msg):
        self.current_pose.header.stamp = msg.header.stamp
        self.current_pose.header.frame_id = self.frame_id
        self.current_pose.pose.orientation.x = msg.pose.orientation.x
        self.current_pose.pose.orientation.y = msg.pose.orientation.y
        self.current_pose.pose.orientation.z = msg.pose.orientation.z
        self.current_pose.pose.orientation.w = msg.pose.orientation.w
        self.current_pose.pose.position.x = msg.pose.position.x
        self.current_pose.pose.position.y = msg.pose.position.y
        self.current_pose.pose.position.z = msg.pose.position.z
        if self.start_locker<100:
            self.current_destination_pose= copy.deepcopy(self.current_pose)
            self.start_locker+=1

    def __check_path_update(self):
        if len(self.current_path.poses)!=len(self.last_path.poses):
            self.last_path.poses=copy.deepcopy(self.current_path.poses)
            return 1
        for i in range(0,len(self.current_path.poses)):#ignore time difference
            self.last_path.poses[i].header.stamp=self.current_path.poses[i].header.stamp
        if self.current_path.poses==self.last_path.poses:
            return 0
        else:
            self.last_path.poses=copy.deepcopy(self.current_path.poses)
            return 1
    def reload_path(self,msg):
        self.current_path.poses=copy.deepcopy(msg.poses)
        if self.__check_path_update():
            self.__is_path_update=1
    def __is_meet_current_destination(self):
        pose_err_x =abs(self.current_pose.pose.position.x-self.path_in_use.poses[self.__path_pointer].pose.position.x)
        pose_err_y=abs(self.current_pose.pose.position.y-self.path_in_use.poses[self.__path_pointer].pose.position.y)
        pose_err_z=abs(self.current_pose.pose.position.z-self.path_in_use.poses[self.__path_pointer].pose.position.z)
        self.distance_err=math.sqrt(pose_err_x**2+pose_err_y**2+pose_err_z**2)
        #print("cur:%f,%f,%f des:%f,%f,%f"%(self.current_pose.pose.position.x,self.current_pose.pose.position.y,self.current_pose.pose.position.z,self.current_path.poses[self.__path_pointer].pose.position.x,self.current_path.poses[self.__path_pointer].pose.position.y,self.current_path.poses[self.__path_pointer].pose.position.z))
        #print("distance_err:%f"%(distance_err))
        if self.distance_err<0.10:
            return 1
        else:
            return 0
    def compute_current_destination(self):
        if self.__is_path_update==1:
            if len( self.path_in_use.poses)==0:#first time to get taget
                self.__path_pointer=0
                self.path_in_use.poses=copy.deepcopy(self.current_path.poses)
                print("New path loaded in first time.")
            else:#some path already in use, check if it should be changed
                self.path_in_use.poses=copy.deepcopy(self.current_path.poses)
                self.__path_pointer=self.get_point_in_path()
            self.__is_path_update=0

        if  self.__path_pointer+1<len(self.path_in_use.poses):
            self.__path_pointer+=self.__is_meet_current_destination()

        if len(self.path_in_use.poses):
            self.current_destination_pose.pose=copy.deepcopy(self.path_in_use.poses[self.__path_pointer].pose)
    def get_current_pointer(self):
        return self.__path_pointer
    def get_point_in_path(self):
        err_list=[]
        for c_pose in self.path_in_use.poses:
             pose_err_x=abs(self.current_pose.pose.position.x-c_pose.pose.position.x)
             pose_err_y=abs(self.current_pose.pose.position.y-c_pose.pose.position.y)
             pose_err_z=abs(self.current_pose.pose.position.z-c_pose.pose.position.z)
             total_err=pose_err_x**2+pose_err_y**2+pose_err_z**2
             err_list.append(total_err)
        return err_list.index(min(err_list))




Path_handler=Flx_Path_wapper()


def Path_Callback(msg):
    global Path_handler
    Path_handler.reload_path(msg)

def PoseStamped_Callback(msg):
    global Path_handler
    Path_handler.reload_pose(msg)


def set_current_destination():
    global Path_handler
    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
        Path_handler.compute_current_destination()
        Path_handler.current_destination_pose.header.stamp = rospy.Time.now()
        Path_handler.current_destination_pose.header.frame_id = Path_handler.frame_id
        if Path_handler.start_locker<100:
             rospy.loginfo('wait for valid path.')
             pass
        else:
             print ("Path done: %d of %d  distance_err:%f" %(Path_handler.get_current_pointer(),len(Path_handler.current_path.poses),Path_handler.distance_err))
             desination_puber.publish(Path_handler.current_destination_pose)
        rate.sleep()



if __name__== '__main__':
    try:
        rospy.init_node('navigation_puber_node', anonymous=True)
        pose_feedback = rospy.Subscriber('/mavros/vision_pose/pose', PoseStamped, PoseStamped_Callback)
        global_plan = rospy.Subscriber("/flx_path_plan", Path , Path_Callback)
        desination_puber = rospy.Publisher("/mavros/setpoint_position/local", PoseStamped, queue_size=10)
        set_current_destination()
    except rospy.ROSInterruptException:
        pass
